package com.exante.homework.common

import scala.collection.mutable

class Candle(val open: Double) {
  var close, low, high, volume: Double = 0
}

object CandleHolders {
  type CandleHolder = mutable.HashMap[String, Candle]
  type StampCandleHolder = mutable.HashMap[Long, CandleHolder]
}
