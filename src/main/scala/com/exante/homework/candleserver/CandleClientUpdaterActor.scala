package com.exante.homework.candleserver

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import com.exante.homework.history.ExchangeHistoryMessages.{GetPreviousMinuteCandles, PreviousMinuteCandles}
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{FiniteDuration, _}

class CandleClientUpdaterActor extends Actor {

  case class CandleClientUpdaterTick()

  override def preStart(): Unit = {
    context.system.scheduler.schedule(durationToNextMinute(), 1.minute, self, CandleClientUpdaterTick)
  }

  def durationToNextMinute(): FiniteDuration = {
    val now: DateTime = DateTime.now
    val nextMinute: DateTime = now.plusMinutes(1).withSecondOfMinute(1)
    FiniteDuration(nextMinute.getMillis - now.getMillis, TimeUnit.MILLISECONDS)
  }

  override def receive: Receive = {
    case CandleClientUpdaterTick =>
      context.actorSelection("/user/app/ExchangeHistoryActor") ! GetPreviousMinuteCandles
    case PreviousMinuteCandles(timestamp, candleHolder) =>
      context.actorSelection("/user/app/CandleServer/*") ! ClientSendCandles(timestamp, candleHolder)
  }
}