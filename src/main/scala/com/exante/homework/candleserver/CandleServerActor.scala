package com.exante.homework.candleserver

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import com.exante.homework.application.ApplicationConfig

class CandleServerActor extends Actor with ActorLogging {
  val serverAddress = ApplicationConfig.candleServer

  implicit lazy val system: ActorSystem = context.system

  override def preStart(): Unit = {
    logMessage("bind to " + serverAddress)
    IO(Tcp) ! Bind(self, serverAddress)
  }

  override def receive: Receive = {
    case _: CommandFailed =>
      logMessage("bind failed")
      context.stop(self)
    case b@Bound(address) =>
      logMessage("bound to " + address)
    case c@Connected(remote, local) =>
      logMessage("new client " + remote)
      val connection = sender()
      val handler = context.actorOf(Props(classOf[CandleSessionClientActor], connection, remote))
      connection ! Register(handler)
    case ClientClosed(remote) => logMessage("client closed " + remote)
  }

  def logMessage(message: String): Unit = log.info("CANDLE SERVER: " + message)
}