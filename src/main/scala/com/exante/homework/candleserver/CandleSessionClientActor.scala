package com.exante.homework.candleserver

import java.net.{InetAddress, InetSocketAddress}
import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{Actor, ActorRef}
import akka.io.Tcp.{ConnectionClosed, Write}
import akka.util.ByteString
import com.exante.homework.common.Candle
import com.exante.homework.common.CandleHolders._
import com.exante.homework.history.ExchangeHistoryMessages.{CandlesHistory, GetCandlesHistory}
import play.api.libs.json.{Json, Writes}

case class ClientSendCandles(timestamp: Long, candle: CandleHolder)

case class ClientClosed(address: InetSocketAddress)

class CandleSessionClientActor(connection: ActorRef, remote: InetSocketAddress) extends Actor {
  override def preStart(): Unit = {
    context.actorSelection("/user/app/ExchangeHistoryActor") ! GetCandlesHistory
  }

  override def receive: Receive = {
    case CandlesHistory(history) =>
      history.foreach { case (timestamp: Long, candleHolder: CandleHolder) => sendCandles(timestamp, candleHolder) }
    case ClientSendCandles(timestamp, candleHolder) =>
      sendCandles(timestamp, candleHolder)
    case _: ConnectionClosed =>
      context.parent ! ClientClosed(remote)
      context.stop(self)
  }

  def sendCandles(timestamp: Long, candleHolder: CandleHolder): Unit = candleHolder.foreach { case (ticker: String, candle: Candle) => sendCandle(ticker, timestamp, candle) }

  def sendCandle(ticker: String, timestamp: Long, candle: Candle): Unit = connection ! Write(ClientCandleSerializer.serializeToJson(ticker, timestamp, candle))
}

object ClientCandleSerializer {
  private val format: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

  case class ClientCandle(ticker: String, date: Date, open: Double, high: Double, low: Double, close: Double, volume: Double)

  implicit val writer = new Writes[ClientCandle] {
    def writes(candle: ClientCandle) = {
      Json.obj(
        "ticker" -> candle.ticker,
        "timestamp" -> format.format(candle.date),
        "open" -> candle.open,
        "high" -> candle.high,
        "low" -> candle.low,
        "close" -> candle.close,
        "volume" -> candle.volume
      )
    }
  }

  def serializeToJson(ticker: String, timestamp: Long, candle: Candle): ByteString = {
    val result = ClientCandle(ticker, new Date(timestamp), candle.open, candle.high, candle.low, candle.close, candle.volume)
    ByteString(Json.toJson(result).toString() + "\n")
  }
}