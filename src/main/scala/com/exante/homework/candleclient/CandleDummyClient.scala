package com.exante.homework.candleclient

import akka.actor.{Actor, ActorLogging, ActorSystem, PoisonPill, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.pattern.{Backoff, BackoffSupervisor}
import com.exante.homework.application.ApplicationConfig

import scala.concurrent.duration._

class CandleClientActor extends Actor with ActorLogging {
  val candleServer = ApplicationConfig.candleServer

  implicit lazy val system: ActorSystem = context.system

  override def preStart(): Unit = {
    log.info("try connect to " + candleServer)
    IO(Tcp) ! Connect(
      remoteAddress = candleServer,
      timeout = Option(10.seconds)
    )
  }

  override def receive: Receive = {
    case _: CommandFailed =>
      log.info("connection failed")
      context.stop(self)
    case c@Connected(remote, local) =>
      log.info("connected to " + remote)
      sender() ! Register(self)
      context become {
        case Received(data) =>
          println(new String(data.toByteBuffer.array()))
        case _: ConnectionClosed =>
          log.info("connection closed")
          context.stop(self)
      }
  }
}

class ClientWatchdogActor extends Actor {
  override def preStart(): Unit = {
    val supervisorProps = BackoffSupervisor.props(
      Backoff.onStop(
        Props(classOf[CandleClientActor]),
        childName = classOf[CandleClientActor].getName,
        minBackoff = 3.seconds,
        maxBackoff = 20.seconds,
        randomFactor = 0.2
      ))
    context.actorOf(supervisorProps)
  }

  override def receive: Receive = {
    case PoisonPill => context.stop(self)
  }
}

object CandleDummyClient {
  def main(args: Array[String]): Unit = {
    akka.Main.main(Array(classOf[ClientWatchdogActor].getName))
  }
}
