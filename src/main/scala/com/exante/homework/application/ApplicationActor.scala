package com.exante.homework.application

import akka.actor.{Actor, PoisonPill, Props}
import akka.pattern.{Backoff, BackoffSupervisor}
import com.exante.homework.candleserver.{CandleClientUpdaterActor, CandleServerActor}
import com.exante.homework.exchangeclient.{ExchangeRawReceiverActor, ExchangeReceiverActor}
import com.exante.homework.history.ExchangeHistoryActor

import scala.concurrent.duration._

class ApplicationActor extends Actor {
  override def preStart(): Unit = {
    val exchangeReceiver = context.actorOf(Props[ExchangeReceiverActor])
    val receiverSupervisorProps = BackoffSupervisor.props(
      Backoff.onStop(
        Props(classOf[ExchangeRawReceiverActor], exchangeReceiver),
        childName = classOf[ExchangeRawReceiverActor].getName,
        minBackoff = 3.seconds,
        maxBackoff = 20.seconds,
        randomFactor = 0.2
      ))
    val receiverSupervisor = context.actorOf(receiverSupervisorProps)

    val history = context.actorOf(Props[ExchangeHistoryActor], "ExchangeHistoryActor")

    val candleServer = context.actorOf(Props[CandleServerActor], "CandleServer")
    val candleClientUpdater = context.actorOf(Props[CandleClientUpdaterActor])
  }

  def receive = {
    case PoisonPill => context.stop(self)
  }
}

