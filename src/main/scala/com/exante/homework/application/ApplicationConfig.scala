package com.exante.homework.application

import java.net.InetSocketAddress

object ApplicationConfig {
  val exchangeServer = new InetSocketAddress("localhost", 5555)
  val candleServer = new InetSocketAddress("localhost", 5252)
}
