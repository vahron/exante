package com.exante.homework.application

object ExanteHomeworkApplication {
  def main(args: Array[String]): Unit = {
    val initialActor = classOf[ApplicationActor].getName
    akka.Main.main(Array(initialActor))
  }
}
