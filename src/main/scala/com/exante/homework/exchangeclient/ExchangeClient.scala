package com.exante.homework.exchangeclient

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import com.exante.homework.application.ApplicationConfig

import scala.concurrent.duration._

class ExchangeRawReceiverActor(exchangeReceiver:ActorRef) extends Actor with ActorLogging {
  val exchangeServerAddress = ApplicationConfig.exchangeServer

  implicit lazy val system: ActorSystem = context.system

  override def preStart(): Unit = {
    logMessage("connect to " + exchangeServerAddress)
    IO(Tcp) ! Connect(
      remoteAddress = exchangeServerAddress,
      timeout = Option(10.seconds)
    )
  }

  override def receive: Receive = {
    case _: CommandFailed =>
      logMessage("connect failed")
      context.stop(self)
    case c@Connected(remote, local) =>
      logMessage("connected to " + remote)
      sender() ! Register(self)
      context become {
        case Received(data) =>
          val exchange = ExchangeSerializer.deserialize(data)
          exchangeReceiver ! NewExchange(exchange)
          println(exchange)
        case _: ConnectionClosed =>
          logMessage("connection closed")
          context.stop(self)
      }
  }

  def logMessage(message:String):Unit = log.info("EXCHANGE: " + message)
}

class ExchangeReceiverActor extends Actor {
  override def receive: Receive = {
    case NewExchange(exchange) =>
      context.actorSelection("/user/app/ExchangeHistoryActor") ! NewExchange(exchange)
  }
}
