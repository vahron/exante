package com.exante.homework.exchangeclient

import java.nio.ByteOrder

import akka.util.ByteString

case class ExchangeType(timestamp: Long, ticker: String, price: Double, size: Integer) {
  override def toString: String = s"$timestamp $ticker $price $size"
}

object ExchangeSerializer {
  implicit val byteOrder: ByteOrder = ByteOrder.BIG_ENDIAN
  
  def deserialize(byteString: ByteString): ExchangeType = {
    val input = byteString.iterator

    val length = input.getShort
    val timestamp = input.getLong
    val tickerLen = input.getShort
    val rawString: Array[Byte] = Array.ofDim(tickerLen)
    input.getBytes(rawString)
    val ticker = new String(rawString)
    val price = input.getDouble
    val size = input.getInt
    ExchangeType(timestamp, ticker, price, size)
  }
}

case class NewExchange(exchange: ExchangeType)

