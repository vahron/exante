package com.exante.homework.history

import akka.actor.Actor
import com.exante.homework.common.Candle
import com.exante.homework.common.CandleHolders._
import com.exante.homework.exchangeclient.{ExchangeType, NewExchange}
import com.exante.homework.history.ExchangeHistoryMessages.{CandlesHistory, GetCandlesHistory, GetPreviousMinuteCandles, PreviousMinuteCandles}
import org.joda.time.DateTime

import scala.collection.mutable
import scala.concurrent.duration._

object ExchangeHistoryMessages {

  case class GetCandlesHistory()

  case class CandlesHistory(history: StampCandleHolder)

  case class GetPreviousMinuteCandles()

  case class PreviousMinuteCandles(timestamp: Long, candle: CandleHolder)

}

class ExchangeHistoryActor extends Actor {
  var history: StampCandleHolder = mutable.HashMap.empty

  override def receive: Receive = {
    case NewExchange(exchange: ExchangeType) =>
      val minute = truncateToMinutes(exchange.timestamp)

      if (!history.contains(minute)) {
        history += (minute -> mutable.HashMap.empty)
      }
      var candleHolder: CandleHolder = history(minute)

      if (!candleHolder.contains(exchange.ticker)) {
        candleHolder += (exchange.ticker -> new Candle(exchange.price))
      }
      val candle: Candle = candleHolder(exchange.ticker)

      candle.close = exchange.price
      candle.low = Math.min(candle.low, exchange.price)
      candle.high = Math.max(candle.high, exchange.price)
      candle.volume += exchange.size

      cleanUpHistory()

    case GetCandlesHistory =>
      cleanUpHistory()
      sender() ! CandlesHistory(history.clone())

    case GetPreviousMinuteCandles =>
      cleanUpHistory()
      val min: Long = previousMin()
      if (history.contains(min)) {
        sender() ! PreviousMinuteCandles(min, history(min).clone())
      }
  }

  def cleanUpHistory(): Unit = {
    val currentMinutes = truncateToMinutes(DateTime.now.getMillis)
    history = history.filterNot {
      case (stamp: Long, h: CandleHolder) =>
        currentMinutes - stamp >= 10.minutes.toMillis
    }
  }

  def previousMin(): Long = truncateToMinutes(DateTime.now.minusMinutes(1).getMillis)

  def truncateToMinutes(timestamp: Long): Long = new DateTime(timestamp).withSecondOfMinute(0).withMillisOfSecond(0).getMillis
}
